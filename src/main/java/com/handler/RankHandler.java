package com.handler;

import com.entity.RankEntity;
import com.manager.RankManager;

import java.util.List;

/***
 * 排行榜功能接口
 */
public class RankHandler {
    /**
     * 获取前后10名玩家排名
     * @param playerId
     * @return
     */
    public static List<RankEntity> getPlayersRank(String activityId,String playerId){
        return RankManager.searchRankList(activityId,playerId);
    }

    /***
     * 增加活动分
     * @param activityId
     * @param playerId
     * @param count
     */
    public static void activityAddScore(String activityId,String playerId,int count){
        RankManager.getInstance().addScore(activityId,playerId,count);
    }

}
