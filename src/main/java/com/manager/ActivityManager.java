package com.manager;

import com.logic.EndActivityTask;
import com.logic.StartActivityTask;

/**
 * 活动管理器
 */
public class ActivityManager {
    /**
     * 开启活动
     * @param activityId 活动id
     * @param days 持续时间
     * @param maxScore 最大得分
     */
    public static void startActivity(String activityId,int days,int maxScore){
        ThreadPoolManager.getInstance().addTask(new StartActivityTask(activityId,maxScore));
        ThreadPoolManager.getInstance().addAsynDelayTask(new EndActivityTask(activityId),days);
    }
}
