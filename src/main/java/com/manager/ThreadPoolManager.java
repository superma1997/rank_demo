package com.manager;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class ThreadPoolManager
{ 		
	private static ThreadPoolManager instance = new ThreadPoolManager();
	public static ThreadPoolManager getInstance()
	{
		return instance;
	}
	
	private ScheduledExecutorService timerService;
	private ExecutorService poolService;  
	
	private ThreadPoolManager()
	{		
		closeTask();
		timerService= new ScheduledThreadPoolExecutor(10);		
		poolService=Executors.newCachedThreadPool();
		
	} 
		
	public void addTask(Runnable task)
	{    	
		poolService.execute(task);
	}
	
	public ScheduledFuture<?> addAsynDelayTask(Runnable task,int days)
	{    	
		return timerService.schedule(task,days,TimeUnit.DAYS);
	}
	
	/**
	 * 
	 * @param task
	 * @param seconds 延迟 seconds几秒执行。
	 * @return
	 */
	public  ScheduledFuture<?> runDelaySeconds(Runnable task,int seconds)
	{
		return timerService.schedule(task,seconds,TimeUnit.SECONDS);	
	}	
		
	public  ScheduledFuture<?> runByRunTimeByDay(Runnable task,long targetTime)
	{	
		long curTime=System.currentTimeMillis();		
		long initialDelay=targetTime-curTime;	
		if(initialDelay>=0)
		{
			return timerService.scheduleWithFixedDelay(task, initialDelay,86400*1000,TimeUnit.MILLISECONDS);
		}
		else
		{
			System.out.println("启动定时器的时候 发现 initialDelay="+initialDelay);
		}
		return null;	
	}
	
	public  ScheduledFuture<?> runByRunTimeByHour(Runnable task,long targetTime)
	{	
		long curTime=System.currentTimeMillis();		
		long initialDelay=targetTime-curTime;	
		if(initialDelay>=0)
		{
			return timerService.scheduleWithFixedDelay(task, initialDelay,60*60*1000,TimeUnit.HOURS);
		}
		else
		{
			System.out.println("启动定时器的时候 发现 initialDelay="+initialDelay);
		}
		return null;	
	}
	


	public  ScheduledFuture<?> runByFixedRateTime(Runnable task,long initialDelay,long fixedRateTime)
	{
		return timerService.scheduleAtFixedRate(task,initialDelay,fixedRateTime,TimeUnit.MILLISECONDS);		
	}
	
	public void closeTask()
	{   
		if(timerService!=null)
		{
			timerService.shutdownNow();
			timerService=null;
		}		
		if(poolService!=null)
		{
			poolService.shutdownNow();
			poolService=null;
		}
					
	}
	
}
