package com.manager;

import com.db.JedisUtil;
import com.entity.RankEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RankManager {
    private static volatile RankManager rankManager = null;
    /**设置一个基准时间2100年，计算公式为(baseTime - curTime)/baseTime得到一个小数与score相加并保存，用于score排序的稳定性*/
    private static final long baseTime = 4102416000000L;

    public static RankManager getInstance(){
        if (rankManager == null){
            synchronized (RankManager.class){
                if (rankManager == null){
                    rankManager = new RankManager();
                }
            }
        }
        return rankManager;
    }

    /**
     * 活动加分
     * @param activity
     * @param playerId
     * @param count
     */
    public synchronized void addScore(String activity,String playerId,int count){
        //加分为0不处理
        if (count == 0){
            return;
        }
        JedisUtil jedisUtil = JedisUtil.getInstance();
        int max = Integer.parseInt(jedisUtil.STRINGS.get(activity));
        double curScore =  jedisUtil.SORTSET.zscore(activity,playerId);
        //已经满分不处理
        if (curScore == max){
            return;
        }
        long curTime = System.currentTimeMillis();
        double finalScore = 0;
        double littleNum = (baseTime - curTime)/baseTime;
        //未超过活动最大分值
        if (curScore + count < max){
            finalScore = (int)curScore + count + littleNum;
            jedisUtil.SORTSET.zadd(activity,finalScore,playerId);
        }else {
            finalScore =  max + littleNum;
            jedisUtil.SORTSET.zadd(activity,finalScore,playerId);
        }
    }

    /**
     * 获取玩家前10和后10的排名信息
     * @param activity
     * @param playerId
     * @return
     */
    public static List<RankEntity> searchRankList(String activity, String playerId){
        JedisUtil jedisUtil = JedisUtil.getInstance();
        //先获取玩家位置索引
        long position = jedisUtil.SORTSET.zrevrank(activity,playerId);
        //注意这里处理索引失效问题
        long upPosition = (position - 10) >= 0 ? (position - 10) : 0;
        Set<String> rankSet  = jedisUtil.SORTSET.zrevrange(activity,upPosition,position + 10);
        List<RankEntity> rankEntities = new ArrayList<>();
        rankSet.forEach(v -> {
            int score = (int) jedisUtil.SORTSET.zscore(activity,v);
            long rank = jedisUtil.SORTSET.zrank(activity,v);
            RankEntity entity = new RankEntity(v,rank,score);
            rankEntities.add(entity);
        });
        return rankEntities;
    }
}
