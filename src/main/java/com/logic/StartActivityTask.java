package com.logic;

import com.db.JedisUtil;

/**
 * 开启活动任务
 */
public class StartActivityTask implements Runnable{
    private String activityId;
    private int maxScore;
    public StartActivityTask(String activityId,int maxScore){
        this.activityId = activityId;
        this.maxScore = maxScore;
    }
    public void run() {
        //保存最大值
        if (JedisUtil.getInstance().STRINGS.get(activityId+"max") == null){
            JedisUtil.getInstance().STRINGS.set(activityId+"max", String.valueOf(maxScore));
        }
    }
}
