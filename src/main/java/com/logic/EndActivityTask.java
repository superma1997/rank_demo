package com.logic;

import com.db.JedisUtil;

/**
 * 结束活动任务
 */
public class EndActivityTask implements Runnable{
    private String activityId;
    public EndActivityTask(String activityId){
        this.activityId = activityId;
    }
    public void run() {
        //清除redis
        JedisUtil.getInstance().KEYS.del(activityId+"max");
        JedisUtil.getInstance().KEYS.del(activityId);
    }
}
