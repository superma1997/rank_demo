package com.entity;

public class RankEntity {
    private String id;
    private long position;
    private int score;
    public RankEntity(){

    }

    public RankEntity(String id, long position, int score) {
        this.id = id;
        this.position = position;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "RankEntity{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", score=" + score +
                '}';
    }
}
